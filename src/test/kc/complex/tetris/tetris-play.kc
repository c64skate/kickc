// Tetris Game for the Commodore 64
// Implementation of the tetris game play logic. Most of the logic is modelled after NES tetris
// Source: https://meatfighter.com/nintendotetrisai/

import "tetris-data"
import "tetris-pieces"

// Pointers to the playfield address for each playfield line
byte*[PLAYFIELD_LINES] playfield_lines;

// Indixes into the playfield  for each playfield line
byte[PLAYFIELD_LINES+1] playfield_lines_idx;

// The index of the next moving piece. (0-6)
byte next_piece_idx = 0;

// The current moving piece. Points to the start of the piece definition.
byte* current_piece = 0;

// The curent piece orientation - each piece have 4 orientations (00/$10/$20/$30).
// The orientation chooses one of the 4 sub-graphics of the piece.
byte current_orientation = 0;

// The speed of moving down the piece when soft-drop is not activated
// This array holds the number of frames per move by level (0-29). For all levels 29+ the value is 1.
const byte[] MOVEDOWN_SLOW_SPEEDS = { 48, 43, 38, 33, 28, 23, 18, 13, 8, 6, 5, 5, 5, 4, 4, 4, 3, 3, 3, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1 };

// The rate of moving down the current piece (number of frames between moves if movedown is  not forced)
byte current_movedown_slow = 48;

// The rate of moving down the current piece fast (number of frames between moves if movedown is not forced)
const byte current_movedown_fast = 10;

// Counts up to the next movedown of current piece
byte current_movedown_counter = 0;

// Base Score values for removing 0-4 lines (in BCD)
// These values are added to score_add_bcd for each level gained.
const dword[] SCORE_BASE_BCD = { $0000, $0040, $0100, $0300, $1200 };

// Score values for removing 0-4 lines (in BCD)
// These values are updated based on the players level and the base values from SCORE_BASE_BCD
dword[5] score_add_bcd;

// Initialize play data tables
void play_init() {
	// Initialize the playfield line pointers;
	byte idx = 0;
	byte* pli = playfield;
	for(byte j:0..PLAYFIELD_LINES-1) {
		playfield_lines[j] = pli;
		playfield_lines_idx[j] = idx;
		pli += PLAYFIELD_COLS;
		idx += PLAYFIELD_COLS;
	}
	playfield_lines_idx[PLAYFIELD_LINES] = PLAYFIELD_COLS*PLAYFIELD_LINES;

	// Set initial speed of moving down a tetromino
	current_movedown_slow = MOVEDOWN_SLOW_SPEEDS[level];
	// Set the initial score add values
    for(byte b: 0..4) {
        score_add_bcd[b] = SCORE_BASE_BCD[b];
    }

}

// Perform any movement of the current piece
// key_event is the next keyboard_event() og $ff if no keyboard event is pending
// Returns a byte signaling whether rendering is needed. (0 no render, >0 render needed)
byte play_movement(byte key_event) {
    byte render = 0;
	render += play_move_down(key_event);
	if(game_over!=0) {
	    return render;
	}
	render += play_move_leftright(key_event);
	render += play_move_rotate(key_event);
	return render;
}

// Move down the current piece
// Return non-zero if a render is needed
byte play_move_down(byte key_event) {
		// Handle moving down current piece
		++current_movedown_counter;
		byte movedown = 0;
		// As soon as space is pressed move down once
		if(key_event==KEY_SPACE) {
			movedown++;
		}
		// While space is held down move down faster
		if(keyboard_event_pressed(KEY_SPACE)!=0) {
			if(current_movedown_counter>=current_movedown_fast) {
				movedown++;
			}
		}
		// Move down slowly otherwise
		if(current_movedown_counter>=current_movedown_slow) {
			movedown++;
		}
		// Attempt movedown
		if(movedown!=0) {
			if(play_collision(current_xpos,current_ypos+1,current_orientation)==COLLISION_NONE) {
				// Move current piece down
				current_ypos++;
			} else {
				// Lock current piece
				play_lock_current();
				// Check for any lines and remove them
				byte removed = play_remove_lines();
				// Tally up the score
				play_update_score(removed);
				// Spawn a new piece
				play_spawn_current();
			}
			current_movedown_counter = 0;
			return 1;
		}
		return 0;
}

// Move left/right or rotate the current piece
// Return non-zero if a render is needed
byte play_move_leftright(byte key_event) {
		// Handle keyboard events
		if(key_event==KEY_COMMA) {
			if(play_collision(current_xpos-1,current_ypos,current_orientation)==COLLISION_NONE) {
				current_xpos--;
				return 1;
			}
		} else if(key_event==KEY_DOT) {
			if(play_collision(current_xpos+1,current_ypos,current_orientation)==COLLISION_NONE) {
				current_xpos++;
				return 1;
			}
		}
		return 0;
}

// Rotate the current piece  based on key-presses
// Return non-zero if a render is needed
byte play_move_rotate(byte key_event) {
	// Handle keyboard events
	byte orientation = $80;
	if(key_event==KEY_Z) {
		orientation = (current_orientation-$10)&$3f;
	} else if(key_event==KEY_X) {
		orientation = (current_orientation+$10)&$3f;
	} else {
		return 0;
	}
	if(play_collision(current_xpos, current_ypos, orientation) == COLLISION_NONE) {
		current_orientation = orientation;
		current_piece_gfx = current_piece + current_orientation;
		return 1;
	}
	return 0;
}

// No collision
const byte COLLISION_NONE = 0;
// Playfield piece collision (cell on top of other cell on the playfield)
const byte COLLISION_PLAYFIELD = 1;
// Bottom collision (cell below bottom of the playfield)
const byte COLLISION_BOTTOM = 2;
// Left side collision (cell beyond the left side of the playfield)
const byte COLLISION_LEFT = 4;
// Right side collision (cell beyond the right side of the playfield)
const byte COLLISION_RIGHT = 8;

// Test if there is a collision between the current piece moved to (x, y) and anything on the playfield or the playfield boundaries
// Returns information about the type of the collision detected
byte play_collision(byte xpos, byte ypos, byte orientation) {
	byte* piece_gfx = current_piece + orientation;
	byte i = 0;
	byte yp = ypos;
	for(byte l:0..3) {
		byte* playfield_line = playfield_lines[yp];
		byte xp = xpos;
		for(byte c:0..3) {
			if(piece_gfx[i++]!=0) {
				if(yp>=PLAYFIELD_LINES) {
					// Below the playfield bottom
					return COLLISION_BOTTOM;
				}
				if((xp&$80)!=0) {
					// Beyond left side of the playfield
					return COLLISION_LEFT;
				}
				if(xp>=PLAYFIELD_COLS) {
					// Beyond left side of the playfield
					return COLLISION_RIGHT;
				}
				if(playfield_line[xp]!=0) {
					// Collision with a playfield cell
					return COLLISION_PLAYFIELD;
				}
			}
			xp++;
		}
		yp++;
	}
	return COLLISION_NONE;
}

// Lock the current piece onto the playfield
void play_lock_current() {
	byte i = 0;
	byte yp = current_ypos;
	for(byte l:0..3) {
		byte* playfield_line = playfield_lines[yp];
		byte xp = current_xpos;
		for(byte c:0..3) {
			if(current_piece_gfx[i++]!=0) {
				playfield_line[xp] = current_piece_char;
			}
			xp++;
		}
		yp++;
	}
}

// Spawn a new piece
// Moves the next piece into the current and spawns a new next piece
void play_spawn_current() {
    // Move next piece into current
	byte current_piece_idx = next_piece_idx;
	current_piece = PIECES[current_piece_idx];
	current_piece_char = PIECES_CHARS[current_piece_idx];
	current_orientation = 0;
	current_piece_gfx = current_piece + current_orientation;
	current_xpos = PIECES_START_X[current_piece_idx];
	current_ypos = PIECES_START_Y[current_piece_idx];
	if(play_collision(current_xpos,current_ypos,current_orientation)==COLLISION_PLAYFIELD) {
	    game_over = 1;
	}

    // Spawn a new next piece
	// Pick a random piece (0-6)
	byte piece_idx = 7;
	while(piece_idx==7) {
		piece_idx = sid_rnd()&7;
	}
	next_piece_idx = piece_idx;

}

// Look through the playfield for lines - and remove any lines found
// Utilizes two cursors on the playfield - one reading cells and one writing cells
// Whenever a full line is detected the writing cursor is instructed to write to the same line once more.
// Returns the number of lines removed
byte play_remove_lines() {
	// Start both cursors at the end of the playfield
	byte r = PLAYFIELD_LINES*PLAYFIELD_COLS-1;
	byte w = PLAYFIELD_LINES*PLAYFIELD_COLS-1;

    byte removed = 0;
	// Read all lines and rewrite them
	for(byte y:0..PLAYFIELD_LINES-1) {
		byte full = 1;
		for(byte x:0..PLAYFIELD_COLS-1) {
			byte c = playfield[r--];
			if(c==0) {
				full = 0;
			}
			playfield[w--] = c;
		}
		// If a line is full then re-write it.
		if(full==1) {
			w = w + PLAYFIELD_COLS;
			removed++;
		}
	}

	// Write zeros in the rest of the lines
	while(w!=$ff) {
		playfield[w--] = 0;
	}
	// Return the number of removed lines
	return removed;
}

// Update the score based on the number of lines removed
void play_update_score(byte removed) {
    if(removed!=0){
        byte lines_before = <lines_bcd&$f0;
        dword add_bcd = score_add_bcd[removed];

        asm { sed }
        lines_bcd += removed;
        score_bcd += add_bcd;
        asm { cld }

        // If line 10-part updated increase the level
        byte lines_after = <lines_bcd&$f0;
        if(lines_before!=lines_after) {
            play_increase_level();
        }
    }
}

// Increase the level
void play_increase_level() {
    // Increase level
    level++;
    // Update speed of moving tetrominos down
    if(level>29) {
        current_movedown_slow = 1;
    } else {
        current_movedown_slow = MOVEDOWN_SLOW_SPEEDS[level];
    }
    // Increase BCD-format level
    level_bcd++;
    if((level_bcd&$f)==$a) {
         // If level low nybble hits $a change to $10
        level_bcd += 6;
    }
    // Increase the score values gained
 	asm { sed }
    for(byte b: 0..4) {
        score_add_bcd[b] += SCORE_BASE_BCD[b];
    }
 	asm { cld }
}

