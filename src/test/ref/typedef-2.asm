.pc = $801 "Basic"
:BasicUpstart(__bbegin)
.pc = $80d "Program"
  .label SCREEN = $400
  .label ptr = 2
__bbegin:
  lda #<$1000
  sta.z ptr
  lda #>$1000
  sta.z ptr+1
  jsr main
  rts
main: {
    lda #<ptr+$32
    sta SCREEN
    rts
}
