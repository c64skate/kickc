package dk.camelot64.kickc.asm;

/** The end of a scope */
public class AsmScopeEnd implements AsmLine {

   private int index;

   public AsmScopeEnd() {
   }

   @Override
   public int getLineBytes() {
      return 0;
   }

   @Override
   public double getLineCycles() {
      return 0;
   }

   @Override
   public String getAsm() {
      return "}";
   }

   @Override
   public int getIndex() {
      return index;
   }

   @Override
   public void setIndex(int index) {
      this.index = index;
   }

   @Override
   public String toString() {
      return getAsm();
   }

}
